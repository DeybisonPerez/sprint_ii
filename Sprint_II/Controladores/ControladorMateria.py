from Modelos.Materia import Materia
from Repositorios.RepositorioMateria import RepositorioMateria


class ControladorMateria():
    def __init__(self):
        self.repositorioMateria = RepositorioMateria()

    def index(self):
        return self.repositorioMateria.findAll()

    def create(self, infoMateria):
        nuevaMateria = Materia(infoMateria)
        return self.repositorioMateria.save(nuevaMateria)

    def show(self, id):
        laMateria = Materia(self.repositorioMateria.findById(id))
        return laMateria.__dict__

    def update(self, id, infoMateria):
        materiaactual = Materia(self.repositorioMateria.findById(id))
        materiaactual.nombre = infoMateria["nombre"]
        materiaactual.creditos = infoMateria["creditos"]
        return self.repositorioMateria.save(materiaactual)

    def delete(self, id):
        return self.repositorioMateria.delete(id)