from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS
import json
from waitress import serve

from Controladores.ControladorDepartamento import ControladorDepartamento


from Controladores.ControladorInscripcion import ControladorInscripcion
from Controladores.ControladorMateria import ControladorMateria
from Controladores.ControladorEstudiante import ControladorEstudiante



app = Flask(__name__)
cors = CORS(app)


miControladorEstudiante = ControladorEstudiante()
miControladorMateria = ControladorMateria()
miControladorInscripcion = ControladorInscripcion()
micontroladorDepartamento = ControladorDepartamento()


@app.route("/departamentos", methods=['GET'])
def getDepartamento():
    json = micontroladorDepartamento.index()
    return jsonify(json)


@app.route("/departamento", methods=['POST'])
def crearDepartamento():
    data = request.get_json()
    json = micontroladorDepartamento.create(data)
    return jsonify(json)


@app.route("/departamento/<string:id>", methods=['GET'])
def getDepartamentoPorId(id):
    json = micontroladorDepartamento.show(id)
    return jsonify(json)


@app.route("/departamentos/<string:id>", methods=['PUT'])
def modificarDepartamento(id):
    data = request.get_json()
    json = micontroladorDepartamento.update(id, data)
    return jsonify(json)


@app.route("/departamento/<string:id>", methods=['DELETE'])
def eliminarDepartamento(id):
    json = micontroladorDepartamento.delete(id)




@app.route("/inscripciones", methods=['GET'])
def getInscripciones():
    json = miControladorInscripcion.index()
    return jsonify(json)


@app.route("/inscripciones", methods=['POST'])
def crearInscripciones():
    data = request.get_json()
    json = miControladorInscripcion.create(data)
    return jsonify(json)


@app.route("/inscripciones/<string:id>", methods=['GET'])
def getInscripcionId(id):
    json = miControladorInscripcion.show(id)
    return jsonify(json)


@app.route("/inscripciones/<string:id>", methods=['PUT'])
def modificarInscripcion(id):
    data = request.get_json()
    json = miControladorInscripcion.update(id, data)
    return jsonify(json)


@app.route("/inscripciones/<string:id>", methods=['DELETE'])
def eliminarInscripcion(id):
    json = miControladorInscripcion.delete(id)
    return jsonify(json)




""" Aqui seran las apis de materia"""
@app.route("/materias",methods=['GET'])
def getMaterias():
    json=miControladorMateria.index()
    return jsonify(json)

@app.route("/materias",methods=['POST'])
def crearMateria():
    data = request.get_json()
    json=miControladorMateria.create(data)
    return jsonify(json)

@app.route("/materias/<string:id>",methods=['GET'])
def getMateria(id):
    json=miControladorMateria.show(id)
    return jsonify(json)

@app.route("/materias/<string:id>",methods=['PUT'])
def modificarMateria(id):
    data = request.get_json()
    json=miControladorMateria.update(id,data)
    return jsonify(json)

@app.route("/materias/<string:id>",methods=['DELETE'])
def eliminarMateria(id):
    json=miControladorMateria.delete(id)
    return jsonify(json)




"""EndPoints Estudiante"""
@app.route("/estudiantes",methods=['GET'])
def getEstudiantes():
    json=miControladorEstudiante.index()
    return jsonify(json)

@app.route("/estudiantes",methods=['POST'])
def crearEstudiante():
    data = request.get_json()
    json=miControladorEstudiante.create(data)
    return jsonify(json)

@app.route("/estudiantes/<string:id>",methods=['GET'])
def getEstudiante(id):
    json=miControladorEstudiante.show(id)
    return jsonify(json)

@app.route("/estudiantes/<string:id>",methods=['PUT'])
def modificarEstudiante(id):
    data = request.get_json()
    json=miControladorEstudiante.update(id,data)
    return jsonify(json)

@app.route("/estudiantes/<string:id>",methods=['DELETE'])
def eliminarEstudiante(id):
    json=miControladorEstudiante.delete(id)
    return jsonify(json)


def loadFileConfing():
    with open('config.json') as f:
        data = json.load(f)
    return data

if __name__ == '__main__':
    dataConfing = loadFileConfing()
    print("Server running: " + "http://" + dataConfing["url-backend"] + ":" + str(dataConfing["port"]))
    serve(app, host=dataConfing["url-backend"], port=dataConfing["port"])



